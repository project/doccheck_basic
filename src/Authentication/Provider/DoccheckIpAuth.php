<?php

namespace Drupal\doccheck_basic\Authentication\Provider;

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Doccheck IP authentication provider.
 */
class DoccheckIpAuth implements AuthenticationProviderInterface {

  const PATHS_EXCLUDED = ['admin', 'user', 'doccheck-login', '_dc_callback'];

  /**
   * A configuration object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The variable containing the user manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs an IP authentication provider object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountInterface $current_user, LoggerInterface $logger, EntityTypeManagerInterface $entityTypeManager) {
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->logger = $logger;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Dependency injection create.
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('config.factory'),
    $container->get('current_user'),
    $container->get('logger'),
    $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function applies(Request $request): bool {
    $config = $this->configFactory->get('config.doccheck_basic');
    $crawler_ip = $config->get('dc_crawler_ip');
    $pathinfo = explode('/', $request->getPathInfo());
    if ($config->get('dc_crawler_autologin') !== TRUE
    || in_array($pathinfo[1], self::PATHS_EXCLUDED)
    || (isset($pathinfo[2]) && in_array($pathinfo[2], self::PATHS_EXCLUDED))
    || $crawler_ip === NULL
    || $request->hasPreviousSession()) {
      return FALSE;
    }
    $ip = $request->getClientIp();

    return in_array($ip, $crawler_ip, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request) {
    $config = $this->configFactory->get('config.doccheck_basic');
    $current_user = $this->currentUser;
    $userManager = $this->entityTypeManager->getStorage('user');
    if (!$current_user->isAnonymous()) {
      return $current_user;
    }

    $crawler_ip = $config->get('dc_crawler_ip');
    $uid = $config->get('dc_user');

    $ip = $request->getClientIp();

    if (\in_array($ip, $crawler_ip, FALSE)) {
      /** @var \Drupal\Core\Session\AccountInterface|NULL $account */
      $account = $userManager->load($uid);
      if (\is_object($account)) {
        user_login_finalize($account);
        return $account;
      }
      else {
        $this->logger->error('User uid: @uid does not exist.', ['@uid' => $uid]);
        throw new AccessDeniedHttpException();
      }
    }
    else {
      throw new AccessDeniedHttpException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function handleException(ExceptionEvent $event) {
    $exception = $event->getThrowable();
    if ($exception instanceof AccessDeniedHttpException) {
      $event->setThrowable(new UnauthorizedHttpException('Invalid IP.', $exception));
      return TRUE;
    }
    return FALSE;
  }

}
