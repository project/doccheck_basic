<?php

namespace Drupal\doccheck_basic\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\doccheck_basic\DoccheckBasicCommon;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'doccheck_basic' block.
 *
 * @Block(
 *   id = "doccheck_basic",
 *   admin_label = @Translation("DocCheck Basic"),
 *   category = @Translation("Login")
 * )
 */
class FormBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The DoccheckBassicCommon service.
   *
   * @var \Drupal\doccheck_basic\DoccheckBasicCommon
   */
  protected $docCheckBasicCommon;

  /**
   * Dependency injection through the constructor.
   *
   * @param array $configuration
   *   The block configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\doccheck_basic\DoccheckBasicCommon $docCheckBasicCommon
   *   The DoccheckBassicCommon service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    DoccheckBasicCommon $docCheckBasicCommon,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->docCheckBasicCommon = $docCheckBasicCommon;
  }

  /**
   * Dependency injection create.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration,
    $plugin_id,
    $plugin_definition,
    $container->get('doccheck_basic.commonservice')
    );
  }

  /**
   * Doccheck login block.
   */
  public function build() {
    return $this->docCheckBasicCommon->doccheckBasicLogin('block');
  }

  /**
   * No caching for login block.
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
