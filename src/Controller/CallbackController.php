<?php

namespace Drupal\doccheck_basic\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\doccheck_basic\DoccheckBasicCommon;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * DocCheck Basic Callback Controller.
 */
class CallbackController extends ControllerBase {

  /**
   * The variable containing the conditions configuration.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The variable containing the current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The variable containing the user manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The variable containing the request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The variable containing the request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The variable containing the KillSwitch service.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * The DoccheckBassicCommon service.
   *
   * @var \Drupal\doccheck_basic\DoccheckBasicCommon
   */
  protected $docCheckBasicCommon;

  /**
   * Dependency injection through the constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack service.
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $killSwitch
   *   The killSwitch service.
   * @param \Drupal\doccheck_basic\DoccheckBasicCommon $docCheckBasicCommon
   *   The DoccheckBassicCommon service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    AccountProxyInterface $currentUser,
    EntityTypeManagerInterface $entityTypeManager,
    RequestStack $requestStack,
    KillSwitch $killSwitch,
    DoccheckBasicCommon $docCheckBasicCommon,
  ) {
    $this->configFactory = $configFactory;
    $this->currentUser = $currentUser;
    $this->entityTypeManager = $entityTypeManager;
    $this->requestStack = $requestStack;
    $this->request = $requestStack->getCurrentRequest();
    $this->killSwitch = $killSwitch;
    $this->docCheckBasicCommon = $docCheckBasicCommon;
  }

  /**
   * Dependency injection create.
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'),
    $container->get('current_user'),
    $container->get('entity_type.manager'),
    $container->get('request_stack'),
    $container->get('page_cache_kill_switch'),
    $container->get('doccheck_basic.commonservice')
    );
  }

  /**
   * Doccheck callback page, redirects to requested URL.
   */
  public function callbackPage() {
    $userManager = $this->entityTypeManager->getStorage('user');
    $config = $this->configFactory->get('config.doccheck_basic');
    $this->killSwitch->trigger();

    $redirect_page = $this->request->getSession()->get('dc_page');
    $this->request->getSession()->remove('dc_page');

    if (!isset($redirect_page) || strlen($redirect_page) == 0) {
      $err_msg = $this->t("No cookie found. DocCheck Basic login cookie error.");
      return self::errorMsg($err_msg);
    }
    if (strlen($config->get('dc_user')) < 1) {
      $err_msg = $this->t('DocCheck login user not set.');
      return self::errorMsg($err_msg);
    }
    if ($this->currentUser->getAccount()->isAnonymous()) {
      /** @var \Drupal\Core\Session\AccountInterface|NULL $dc_user */
      $dc_user = $userManager->load($config->get('dc_user'));
      if ($dc_user === NULL) {
        $err_msg = $this->t('DocCheck login username not valid or not selected.');
        return self::errorMsg($err_msg);
      }
      if (in_array('administrator', $dc_user->getRoles())) {
        $err_msg = $this->t('DocCheck login user has administrator role.');
        return self::errorMsg($err_msg);
      }
      user_login_finalize($dc_user);
    }

    $response = new RedirectResponse($redirect_page);
    $response->send();
    return $response;
  }

  /**
   * Defines login page.
   */
  public function loginPage() {
    return $this->docCheckBasicCommon->doccheckBasicLogin('page');
  }

  /**
   * Generate error message.
   */
  public function errorMsg($err_msg) {
    $this->getLogger('doccheck_basic')->error($err_msg);
    $this->messenger()->addMessage($err_msg, 'error');
    return [
      '#type' => 'markup',
      '#markup' => '',
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
