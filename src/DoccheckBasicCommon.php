<?php

namespace Drupal\doccheck_basic;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\doccheck_basic\Form\SettingsForm;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Handles the common logic of Login Block and Login Page.
 */
class DoccheckBasicCommon implements ContainerFactoryPluginInterface {
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * The variable containing the conditions configuration.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The variable containing the current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The variable containing the request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The variable containing the language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * The variable containing the KillSwitch service.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * Dependency injection through the constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $killSwitch
   *   The killSwitch service.
   */
  public function __construct(
    LoggerInterface $logger,
    ConfigFactoryInterface $configFactory,
    AccountProxyInterface $currentUser,
    RequestStack $requestStack,
    LanguageManagerInterface $languageManager,
    RedirectDestinationInterface $redirect_destination,
    KillSwitch $killSwitch,
  ) {
    $this->logger = $logger;
    $this->configFactory = $configFactory;
    $this->currentUser = $currentUser;
    $this->requestStack = $requestStack;
    $this->languageManager = $languageManager;
    $this->redirectDestination = $redirect_destination;
    $this->killSwitch = $killSwitch;
  }

  /**
   * Dependency injection create.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($container->get('logger'),
    $container->get('config.factory'),
    $container->get('current_user'),
    $container->get('request_stack'),
    $container->get('language_manager'),
    $container->get('page_cache_kill_switch')
    );
  }

  /**
   * Defines doccheck login for block and page.
   */
  public function doccheckBasicLogin($template) {
    /** @var \Drupal\Core\Config\ImmutableConfig */
    $config = $this->configFactory->get('config.doccheck_basic');
    /** @var \Symfony\Component\HttpFoundation\Request */
    $request = $this->requestStack->getCurrentRequest();

    $this->killSwitch->trigger();
    if (strlen($config->get('dc_loginid')) < 1) {
      $err_msg = $this->t('DocCheck Login ID not set');
      $this->logger->error($err_msg);
      $this->messenger()->addMessage($err_msg, 'error');
      return [
        '#markup' => '',
        '#cache' => [
          'max-age' => 0,
        ],
      ];
    }

    if ($this->currentUser->getAccount()->isAnonymous()) {
      $request->getSession()->set('forced', TRUE);
    }
    elseif ($template === 'page') {
      return [
        '#theme' => 'doccheck_basic',
        '#loggedin' => TRUE,
        '#dctemplate' => NULL,
        '#width' => NULL,
        '#height' => NULL,
        '#language' => NULL,
        '#loginid' => NULL,
        '#devmode' => NULL,
        '#cache' => [
          'max-age' => 0,
        ],
      ];

    }
    if ($template === 'page' && $config->get('dc_noderedirect') !== '') {
      $dc_page = $config->get('dc_noderedirect');
    }
    elseif (substr($this->redirectDestination->get(), 0, 1) === '/') {
      $dc_page = $this->redirectDestination->get();
    }
    else {
      $dc_page = '/' . $this->redirectDestination->get();
    }
    $request->getSession()->set('dc_page', $dc_page);

    return [
      '#theme' => 'doccheck_basic',
      '#loggedin' => FALSE,
      '#dctemplate' => ($config->get('dc_template') == SettingsForm::CUSTOM_TEMPLATE) ? ($config->get('dc_template_custom')) : ($config->get('dc_template')),
      '#width' => $this->templateSize($config->get('dc_template'), 'w'),
      '#height' => $this->templateSize($config->get('dc_template'), 'h'),
      '#language' => $this->languageManager->getCurrentLanguage()->getId(),
      '#loginid' => $config->get('dc_loginid'),
      '#devmode' => ($config->get('dc_devmode')) ?
      ('<a href="' . $request->getSchemeAndHttpHost() . Url::fromRoute('doccheck_basic.callback')->toString() . '">' . $this->t('Development mode login') . '</a>') :
      (''),
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Returns width or height of doccheck template.
   */
  public function templateSize($type, $dimension) {
    /** @var \Drupal\Core\Config\ImmutableConfig */
    $config = $this->configFactory->get('config.doccheck_basic');
    $template_size = [
      's_red' => ['w' => 156, 'h' => 203],
      'm_red' => ['w' => 311, 'h' => 188],
      'l_red' => ['w' => 424, 'h' => 215],
      'xl_red' => ['w' => 467, 'h' => 231],
      'login_s' => ['w' => 156, 'h' => 203],
      'login_m' => ['w' => 311, 'h' => 195],
      'login_l' => ['w' => 424, 'h' => 215],
      'login_xl' => ['w' => 467, 'h' => 231],
      SettingsForm::CUSTOM_TEMPLATE => [
        'w' => $config->get('dc_template_custom_width'),
        'h' => $config->get('dc_template_custom_height'),
      ],
    ];
    return (isset($template_size[$type])) ?
      ($template_size[$type][$dimension]) : (FALSE);
  }

}
